@extends('la.layouts.auth')

@section('htmlheader_title')
    Password recovery
@endsection

@section('content')

<body>
  <p>
Name: {{ $name }}
</p>

<p>
Email: {{ $email }}
</p>

<p>
Message: {{ $user_message }}
</p>
</body>

@endsection
