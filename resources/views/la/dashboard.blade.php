@extends('la.layouts.app')

@section('htmlheader_title') Dashboard @endsection
@section('contentheader_title') Dashboard @endsection
@section('contentheader_description') Overview @endsection

@section('main-content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

  <section class="content">


  <div class="container-fluid">



            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="https://cdn6.aptoide.com/imgs/a/2/c/a2c70e2d218ac53cacba906860264ee2_icon.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>OCR service</h4>
              <span class="text-muted">convert your photos to texts.</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <a href="{{ url('/admin/maps') }}"><img src="http://www.myiconfinder.com/uploads/iconsets/256-256-79a6cc671eb7205ea4903436e08851c4-map.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail"></a>
              <h4><a href="{{ url('/admin/maps') }}">Areas</a></h4>
              <span class="text-muted">See your role on local areas.</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="https://findicons.com/files/icons/1590/gloss_d/512/mag_glass_512x512x32.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Documents</h4>
              <span class="text-muted">Find your all documents that OCR exported.</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <a href="{{ url('/admin/modules') }}"><img src="https://www.macosicongallery.com/icons/folder-designer-create-custom-folder-icons-2017-08-09/512.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail"></a>
              <h4><a href="{{ url('/admin/modules') }}">Modules</a></h4>
              <span class="text-muted">Create your own modules.</span>
            </div>
          </div>

<br><br>
<div id="calendar_basic" style="width: 1000px; height: 350px;"></div>


<h2 class="sub-header">To-Do Lists</h2>
          <table class="table table-striped">
    <div class="table responsive">
        <thead>
            <tr>
              <th>#ID</th>
              <th>Date</th>
              <th>To-Do</th>
              <th>Status</th>

            </tr>
        </thead>
        <tbody>

          <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "laraadmin2";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);




// Check connection
if ($conn->connect_error) {
   die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id, date, todo, complete, Picture FROM memos";
$result = $conn->query($sql);

if ($result->num_rows > 0) {

   // output data of each row
   while($row = $result->fetch_assoc()) {
     echo '<tr>
 <td scope="row">' . $row['id'] . '</td>
 <td>' . $row['date'] . '</td>
 <td>' . $row['todo'] . '</td>
 <td>' . $row['complete'] . '</td>

</tr>';
   }
} else {
   echo "0 results";
}
echo "</table>";
$conn->close();
?>



        </div>
      </div>
    </div>
    <?php
    ?>

</section>
@endsection

@push('scripts')
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="https://getbootstrap.com/docs/3.3/assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="https://getbootstrap.com/docs/3.3/dist/js/bootstrap.min.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="https://getbootstrap.com/docs/3.3/assets/js/vendor/holder.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="https://getbootstrap.com/docs/3.3/assets/js/ie10-viewport-bug-workaround.js"></script>
@endpush

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush
