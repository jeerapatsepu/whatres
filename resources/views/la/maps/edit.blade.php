@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/maps') }}">Map</a> :
@endsection
@section("contentheader_description", $map->$view_col)
@section("section", "Maps")
@section("section_url", url(config('laraadmin.adminRoute') . '/maps'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Maps Edit : ".$map->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($map, ['route' => [config('laraadmin.adminRoute') . '.maps.update', $map->id ], 'method'=>'PUT', 'id' => 'map-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'test')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/maps') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#map-edit-form").validate({
		
	});
});
</script>
@endpush
