@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/memos') }}">Memo</a> :
@endsection
@section("contentheader_description", $memo->$view_col)
@section("section", "Memos")
@section("section_url", url(config('laraadmin.adminRoute') . '/memos'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Memos Edit : ".$memo->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($memo, ['route' => [config('laraadmin.adminRoute') . '.memos.update', $memo->id ], 'method'=>'PUT', 'id' => 'memo-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'todo')
					@la_input($module, 'date')
					@la_input($module, 'Picture')
					@la_input($module, 'complete')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/memos') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#memo-edit-form").validate({
		
	});
});
</script>
@endpush
