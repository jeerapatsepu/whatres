-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 27, 2018 at 04:17 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laraadmin2`
--

-- --------------------------------------------------------

--
-- Table structure for table `backups`
--

CREATE TABLE `backups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `backup_size` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date` date NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `Profile image` int(11) NOT NULL,
  `Phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`id`, `name`, `email`, `message`, `created_at`, `updated_at`) VALUES
(1, 'r', 'rr@thrtd.com', 'erter', '2018-10-27 04:27:35', '2018-10-27 04:27:35'),
(2, 'ewtrwerwer', 'jeerapatsepu@gmail.com', 'regergerg', '2018-10-27 04:28:37', '2018-10-27 04:28:37');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `color` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `tags`, `color`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Administration', '[]', '#000', NULL, '2018-10-18 05:51:53', '2018-10-18 05:51:53'),
(2, 'Admin', '[]', 'orange', '2018-10-20 07:20:42', '2018-10-20 07:17:48', '2018-10-20 07:20:42');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `designation` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Male',
  `mobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile2` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dept` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_birth` date NOT NULL DEFAULT '1990-01-01',
  `date_hire` date NOT NULL,
  `date_left` date NOT NULL DEFAULT '1990-01-01',
  `salary_cur` decimal(15,3) NOT NULL DEFAULT '0.000',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `designation`, `gender`, `mobile`, `mobile2`, `email`, `dept`, `city`, `address`, `about`, `date_birth`, `date_hire`, `date_left`, `salary_cur`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Super Admin', 'Male', '8888888888', '', 'jeerapatsepu@gmail.com', 1, 'Pune', 'Karve nagar, Pune 411030', 'About user / biography', '2018-10-18', '2018-10-18', '2018-10-18', '0.000', NULL, '2018-10-18 05:52:57', '2018-10-18 05:52:57');

-- --------------------------------------------------------

--
-- Table structure for table `helps`
--

CREATE TABLE `helps` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `test` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `helps`
--

INSERT INTO `helps` (`id`, `deleted_at`, `created_at`, `updated_at`, `test`) VALUES
(1, NULL, '2018-10-27 05:02:08', '2018-10-27 05:02:08', 'This is test message');

-- --------------------------------------------------------

--
-- Table structure for table `la_configs`
--

CREATE TABLE `la_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_configs`
--

INSERT INTO `la_configs` (`id`, `key`, `section`, `value`, `created_at`, `updated_at`) VALUES
(1, 'sitename', '', 'Whatres', '2018-10-18 05:51:57', '2018-10-21 02:33:35'),
(2, 'sitename_part1', '', 'WR', '2018-10-18 05:51:57', '2018-10-21 02:33:35'),
(3, 'sitename_part2', '', 'Whatres', '2018-10-18 05:51:58', '2018-10-21 02:33:35'),
(4, 'sitename_short', '', 'WR', '2018-10-18 05:51:58', '2018-10-21 02:33:35'),
(5, 'site_description', '', 'Services: OCR', '2018-10-18 05:51:58', '2018-10-21 02:33:35'),
(6, 'sidebar_search', '', '0', '2018-10-18 05:51:58', '2018-10-21 02:33:35'),
(7, 'show_messages', '', '0', '2018-10-18 05:51:58', '2018-10-21 02:33:35'),
(8, 'show_notifications', '', '0', '2018-10-18 05:51:58', '2018-10-21 02:33:35'),
(9, 'show_tasks', '', '0', '2018-10-18 05:51:58', '2018-10-21 02:33:35'),
(10, 'show_rightsidebar', '', '0', '2018-10-18 05:51:58', '2018-10-21 02:33:35'),
(11, 'skin', '', 'skin-green', '2018-10-18 05:51:58', '2018-10-21 02:33:35'),
(12, 'layout', '', 'fixed', '2018-10-18 05:51:58', '2018-10-21 02:33:35'),
(13, 'default_email', '', 'jsripumngoen@gmail.com', '2018-10-18 05:51:58', '2018-10-21 02:33:35');

-- --------------------------------------------------------

--
-- Table structure for table `la_menus`
--

CREATE TABLE `la_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'module',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hierarchy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_menus`
--

INSERT INTO `la_menus` (`id`, `name`, `url`, `icon`, `type`, `parent`, `hierarchy`, `created_at`, `updated_at`) VALUES
(1, 'Team', '#', 'fa-group', 'custom', 0, 1, '2018-10-18 05:51:52', '2018-10-18 05:51:52'),
(2, 'Users', 'users', 'fa-group', 'module', 1, 0, '2018-10-18 05:51:52', '2018-10-18 05:51:52'),
(3, 'Uploads', 'uploads', 'fa-files-o', 'module', 0, 0, '2018-10-18 05:51:52', '2018-10-18 05:51:52'),
(4, 'Departments', 'departments', 'fa-tags', 'module', 1, 0, '2018-10-18 05:51:52', '2018-10-18 05:51:52'),
(5, 'Employees', 'employees', 'fa-group', 'module', 1, 0, '2018-10-18 05:51:53', '2018-10-18 05:51:53'),
(6, 'Roles', 'roles', 'fa-user-plus', 'module', 1, 0, '2018-10-18 05:51:53', '2018-10-18 05:51:53'),
(8, 'Permissions', 'permissions', 'fa-magic', 'module', 1, 0, '2018-10-18 05:51:53', '2018-10-18 05:51:53'),
(14, 'Memos', 'memos', 'fa fa-cube', 'module', 0, 0, '2018-10-20 06:41:31', '2018-10-20 06:41:31'),
(16, 'Contacts', 'contacts', 'fa fa-users', 'module', 0, 0, '2018-10-20 07:22:13', '2018-10-20 07:22:13'),
(17, 'Helps', 'helps', 'fa fa-book', 'module', 0, 0, '2018-10-22 01:48:38', '2018-10-22 01:48:38');

-- --------------------------------------------------------

--
-- Table structure for table `memos`
--

CREATE TABLE `memos` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `todo` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL DEFAULT '1970-01-01',
  `Picture` int(11) NOT NULL,
  `complete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=tis620;

--
-- Dumping data for table `memos`
--

INSERT INTO `memos` (`id`, `deleted_at`, `created_at`, `updated_at`, `todo`, `date`, `Picture`, `complete`) VALUES
(2, NULL, '2018-10-27 02:56:39', '2018-10-27 03:53:10', 'Dinner with', '2018-10-27', 0, 0),
(3, NULL, '2018-10-27 03:45:57', '2018-10-27 03:53:24', 'nothong to do', '2018-10-27', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2018_10_20_101405_create_contact_us_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_db` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `view_col` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fa_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `is_gen` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `label`, `name_db`, `view_col`, `model`, `controller`, `fa_icon`, `is_gen`, `created_at`, `updated_at`) VALUES
(1, 'Users', 'Users', 'users', 'name', 'User', 'UsersController', 'fa-group', 1, '2018-10-18 05:51:29', '2018-10-18 05:51:58'),
(2, 'Uploads', 'Uploads', 'uploads', 'name', 'Upload', 'UploadsController', 'fa-files-o', 1, '2018-10-18 05:51:31', '2018-10-18 05:51:58'),
(3, 'Departments', 'Departments', 'departments', 'name', 'Department', 'DepartmentsController', 'fa-tags', 1, '2018-10-18 05:51:32', '2018-10-18 05:51:58'),
(4, 'Employees', 'Employees', 'employees', 'name', 'Employee', 'EmployeesController', 'fa-group', 1, '2018-10-18 05:51:33', '2018-10-18 05:51:58'),
(5, 'Roles', 'Roles', 'roles', 'name', 'Role', 'RolesController', 'fa-user-plus', 1, '2018-10-18 05:51:35', '2018-10-18 05:51:58'),
(7, 'Backups', 'Backups', 'backups', 'name', 'Backup', 'BackupsController', 'fa-hdd-o', 1, '2018-10-18 05:51:42', '2018-10-18 05:51:58'),
(8, 'Permissions', 'Permissions', 'permissions', 'name', 'Permission', 'PermissionsController', 'fa-magic', 1, '2018-10-18 05:51:44', '2018-10-18 05:51:59'),
(14, 'Memos', 'Memos', 'memos', 'todo', 'Memo', 'MemosController', 'fa-cube', 1, '2018-10-20 06:40:34', '2018-10-20 06:41:31'),
(16, 'Contacts', 'Contacts', 'contacts', 'date', 'Contact', 'ContactsController', 'fa-users', 1, '2018-10-20 07:21:40', '2018-10-20 07:22:13'),
(17, 'Helps', 'Helps', 'helps', 'test', 'Help', 'HelpsController', 'fa-book', 1, '2018-10-22 01:47:27', '2018-10-22 01:48:38');

-- --------------------------------------------------------

--
-- Table structure for table `module_fields`
--

CREATE TABLE `module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `colname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module` int(10) UNSIGNED NOT NULL,
  `field_type` int(10) UNSIGNED NOT NULL,
  `unique` tinyint(1) NOT NULL DEFAULT '0',
  `defaultvalue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minlength` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `maxlength` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `popup_vals` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_fields`
--

INSERT INTO `module_fields` (`id`, `colname`, `label`, `module`, `field_type`, `unique`, `defaultvalue`, `minlength`, `maxlength`, `required`, `popup_vals`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'name', 'Name', 1, 16, 0, '', 5, 250, 1, '', 0, '2018-10-18 05:51:29', '2018-10-18 05:51:29'),
(2, 'context_id', 'Context', 1, 13, 0, '0', 0, 0, 0, '', 0, '2018-10-18 05:51:29', '2018-10-18 05:51:29'),
(3, 'email', 'Email', 1, 8, 1, '', 0, 250, 0, '', 0, '2018-10-18 05:51:29', '2018-10-18 05:51:29'),
(4, 'password', 'Password', 1, 17, 0, '', 6, 250, 1, '', 0, '2018-10-18 05:51:29', '2018-10-18 05:51:29'),
(5, 'type', 'User Type', 1, 7, 0, 'Employee', 0, 0, 0, '[\"Employee\",\"Client\"]', 0, '2018-10-18 05:51:29', '2018-10-18 05:51:29'),
(6, 'name', 'Name', 2, 16, 0, '', 5, 250, 1, '', 0, '2018-10-18 05:51:31', '2018-10-18 05:51:31'),
(7, 'path', 'Path', 2, 19, 0, '', 0, 250, 0, '', 0, '2018-10-18 05:51:31', '2018-10-18 05:51:31'),
(8, 'extension', 'Extension', 2, 19, 0, '', 0, 20, 0, '', 0, '2018-10-18 05:51:31', '2018-10-18 05:51:31'),
(9, 'caption', 'Caption', 2, 19, 0, '', 0, 250, 0, '', 0, '2018-10-18 05:51:31', '2018-10-18 05:51:31'),
(10, 'user_id', 'Owner', 2, 7, 0, '1', 0, 0, 0, '@users', 0, '2018-10-18 05:51:31', '2018-10-18 05:51:31'),
(11, 'hash', 'Hash', 2, 19, 0, '', 0, 250, 0, '', 0, '2018-10-18 05:51:31', '2018-10-18 05:51:31'),
(12, 'public', 'Is Public', 2, 2, 0, '0', 0, 0, 0, '', 0, '2018-10-18 05:51:31', '2018-10-18 05:51:31'),
(13, 'name', 'Name', 3, 16, 1, '', 1, 250, 1, '', 0, '2018-10-18 05:51:32', '2018-10-18 05:51:32'),
(14, 'tags', 'Tags', 3, 20, 0, '[]', 0, 0, 0, '', 0, '2018-10-18 05:51:32', '2018-10-18 05:51:32'),
(15, 'color', 'Color', 3, 19, 0, '', 0, 50, 1, '', 0, '2018-10-18 05:51:32', '2018-10-18 05:51:32'),
(16, 'name', 'Name', 4, 16, 0, '', 5, 250, 1, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(17, 'designation', 'Designation', 4, 19, 0, '', 0, 50, 1, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(18, 'gender', 'Gender', 4, 18, 0, 'Male', 0, 0, 1, '[\"Male\",\"Female\"]', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(19, 'mobile', 'Mobile', 4, 14, 0, '', 10, 20, 1, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(20, 'mobile2', 'Alternative Mobile', 4, 14, 0, '', 10, 20, 0, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(21, 'email', 'Email', 4, 8, 1, '', 5, 250, 1, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(22, 'dept', 'Department', 4, 7, 0, '0', 0, 0, 1, '@departments', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(23, 'city', 'City', 4, 19, 0, '', 0, 50, 0, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(24, 'address', 'Address', 4, 1, 0, '', 0, 1000, 0, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(25, 'about', 'About', 4, 19, 0, '', 0, 0, 0, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(26, 'date_birth', 'Date of Birth', 4, 4, 0, '1990-01-01', 0, 0, 0, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(27, 'date_hire', 'Hiring Date', 4, 4, 0, 'date(\'Y-m-d\')', 0, 0, 0, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(28, 'date_left', 'Resignation Date', 4, 4, 0, '1990-01-01', 0, 0, 0, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(29, 'salary_cur', 'Current Salary', 4, 6, 0, '0.0', 0, 2, 0, '', 0, '2018-10-18 05:51:33', '2018-10-18 05:51:33'),
(30, 'name', 'Name', 5, 16, 1, '', 1, 250, 1, '', 0, '2018-10-18 05:51:35', '2018-10-18 05:51:35'),
(31, 'display_name', 'Display Name', 5, 19, 0, '', 0, 250, 1, '', 0, '2018-10-18 05:51:35', '2018-10-18 05:51:35'),
(32, 'description', 'Description', 5, 21, 0, '', 0, 1000, 0, '', 0, '2018-10-18 05:51:35', '2018-10-18 05:51:35'),
(33, 'parent', 'Parent Role', 5, 7, 0, '1', 0, 0, 0, '@roles', 0, '2018-10-18 05:51:35', '2018-10-18 05:51:35'),
(34, 'dept', 'Department', 5, 7, 0, '1', 0, 0, 0, '@departments', 0, '2018-10-18 05:51:35', '2018-10-18 05:51:35'),
(46, 'name', 'Name', 7, 16, 1, '', 0, 250, 1, '', 0, '2018-10-18 05:51:43', '2018-10-18 05:51:43'),
(47, 'file_name', 'File Name', 7, 19, 1, '', 0, 250, 1, '', 0, '2018-10-18 05:51:43', '2018-10-18 05:51:43'),
(48, 'backup_size', 'File Size', 7, 19, 0, '0', 0, 10, 1, '', 0, '2018-10-18 05:51:43', '2018-10-18 05:51:43'),
(49, 'name', 'Name', 8, 16, 1, '', 1, 250, 1, '', 0, '2018-10-18 05:51:44', '2018-10-18 05:51:44'),
(50, 'display_name', 'Display Name', 8, 19, 0, '', 0, 250, 1, '', 0, '2018-10-18 05:51:44', '2018-10-18 05:51:44'),
(51, 'description', 'Description', 8, 21, 0, '', 0, 1000, 0, '', 0, '2018-10-18 05:51:44', '2018-10-18 05:51:44'),
(66, 'todo', 'Todo', 14, 19, 1, '', 6, 1000, 0, '', 0, '2018-10-20 06:41:22', '2018-10-20 06:41:22'),
(67, 'date', 'Date', 14, 4, 0, '', 0, 0, 1, '', 0, '2018-10-20 06:41:55', '2018-10-20 06:41:55'),
(68, 'Picture', 'img', 14, 12, 0, '', 0, 0, 0, '', 0, '2018-10-20 06:42:23', '2018-10-20 06:42:23'),
(69, 'complete', 'Complete', 14, 2, 0, '', 0, 0, 1, '', 0, '2018-10-20 06:43:07', '2018-10-20 06:43:07'),
(74, 'date', 'Date', 16, 4, 0, '', 0, 0, 0, '', 0, '2018-10-20 07:22:07', '2018-10-20 07:22:07'),
(75, 'name', 'Name', 16, 16, 1, '', 6, 256, 1, '', 0, '2018-10-20 07:22:42', '2018-10-20 07:22:42'),
(77, 'name id', 'Name_ID', 16, 19, 0, '', 3, 20, 0, '', 0, '2018-10-20 07:25:43', '2018-10-20 07:25:43'),
(78, 'email', 'Email', 16, 8, 0, '', 6, 256, 0, '', 0, '2018-10-20 07:26:24', '2018-10-20 07:26:24'),
(79, 'Profile image', 'Profile image', 16, 12, 0, '', 0, 0, 0, '', 0, '2018-10-20 07:27:33', '2018-10-20 07:28:03'),
(80, 'Phone', 'Phone', 16, 19, 0, '', 2, 15, 0, '', 0, '2018-10-20 07:28:59', '2018-10-20 07:28:59'),
(81, 'test', 'test', 17, 1, 0, '', 0, 256, 0, '', 0, '2018-10-22 01:48:33', '2018-10-22 01:48:33');

-- --------------------------------------------------------

--
-- Table structure for table `module_field_types`
--

CREATE TABLE `module_field_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_field_types`
--

INSERT INTO `module_field_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Address', '2018-10-18 05:51:23', '2018-10-18 05:51:23'),
(2, 'Checkbox', '2018-10-18 05:51:23', '2018-10-18 05:51:23'),
(3, 'Currency', '2018-10-18 05:51:23', '2018-10-18 05:51:23'),
(4, 'Date', '2018-10-18 05:51:23', '2018-10-18 05:51:23'),
(5, 'Datetime', '2018-10-18 05:51:24', '2018-10-18 05:51:24'),
(6, 'Decimal', '2018-10-18 05:51:24', '2018-10-18 05:51:24'),
(7, 'Dropdown', '2018-10-18 05:51:24', '2018-10-18 05:51:24'),
(8, 'Email', '2018-10-18 05:51:24', '2018-10-18 05:51:24'),
(9, 'File', '2018-10-18 05:51:24', '2018-10-18 05:51:24'),
(10, 'Float', '2018-10-18 05:51:24', '2018-10-18 05:51:24'),
(11, 'HTML', '2018-10-18 05:51:24', '2018-10-18 05:51:24'),
(12, 'Image', '2018-10-18 05:51:24', '2018-10-18 05:51:24'),
(13, 'Integer', '2018-10-18 05:51:24', '2018-10-18 05:51:24'),
(14, 'Mobile', '2018-10-18 05:51:24', '2018-10-18 05:51:24'),
(15, 'Multiselect', '2018-10-18 05:51:24', '2018-10-18 05:51:24'),
(16, 'Name', '2018-10-18 05:51:25', '2018-10-18 05:51:25'),
(17, 'Password', '2018-10-18 05:51:25', '2018-10-18 05:51:25'),
(18, 'Radio', '2018-10-18 05:51:25', '2018-10-18 05:51:25'),
(19, 'String', '2018-10-18 05:51:25', '2018-10-18 05:51:25'),
(20, 'Taginput', '2018-10-18 05:51:25', '2018-10-18 05:51:25'),
(21, 'Textarea', '2018-10-18 05:51:25', '2018-10-18 05:51:25'),
(22, 'TextField', '2018-10-18 05:51:25', '2018-10-18 05:51:25'),
(23, 'URL', '2018-10-18 05:51:25', '2018-10-18 05:51:25'),
(24, 'Files', '2018-10-18 05:51:25', '2018-10-18 05:51:25');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jeerapatsepu@gmail.com', 'f2bab8388b8828960f7b4ad5aa6386c6608b8ac5d497bb4623ce97ced615c667', '2018-10-19 23:58:29');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN_PANEL', 'Admin Panel', 'Admin Panel Permission', NULL, '2018-10-18 05:51:57', '2018-10-18 05:51:57'),
(2, 'ADMIN_PANEL2', 'Admin Panel 2', '', '2018-10-20 07:20:11', '2018-10-20 07:19:19', '2018-10-20 07:20:11');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `dept` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `parent`, `dept`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SUPER_ADMIN', 'Super Admin', 'Full Access Role', 1, 1, NULL, '2018-10-18 05:51:53', '2018-10-18 05:51:53'),
(2, 'ADMIN', 'Admin', '', 2, 2, '2018-10-20 07:20:17', '2018-10-20 07:16:27', '2018-10-20 07:20:17');

-- --------------------------------------------------------

--
-- Table structure for table `role_module`
--

CREATE TABLE `role_module` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `acc_view` tinyint(1) NOT NULL,
  `acc_create` tinyint(1) NOT NULL,
  `acc_edit` tinyint(1) NOT NULL,
  `acc_delete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module`
--

INSERT INTO `role_module` (`id`, `role_id`, `module_id`, `acc_view`, `acc_create`, `acc_edit`, `acc_delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2018-10-18 05:51:53', '2018-10-18 05:51:53'),
(2, 1, 2, 1, 1, 1, 1, '2018-10-18 05:51:54', '2018-10-18 05:51:54'),
(3, 1, 3, 1, 1, 1, 1, '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(4, 1, 4, 1, 1, 1, 1, '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(5, 1, 5, 1, 1, 1, 1, '2018-10-18 05:51:56', '2018-10-18 05:51:56'),
(7, 1, 7, 1, 1, 1, 1, '2018-10-18 05:51:57', '2018-10-18 05:51:57'),
(8, 1, 8, 1, 1, 1, 1, '2018-10-18 05:51:57', '2018-10-18 05:51:57'),
(14, 1, 14, 1, 1, 1, 1, '2018-10-20 06:41:31', '2018-10-20 06:41:31'),
(16, 2, 1, 1, 0, 0, 0, '2018-10-20 07:16:27', '2018-10-20 07:16:27'),
(17, 2, 2, 1, 0, 0, 0, '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(18, 2, 3, 1, 0, 0, 0, '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(19, 2, 4, 1, 0, 0, 0, '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(20, 2, 5, 1, 0, 0, 0, '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(22, 2, 7, 1, 0, 0, 0, '2018-10-20 07:16:30', '2018-10-20 07:16:30'),
(23, 2, 8, 1, 0, 0, 0, '2018-10-20 07:16:31', '2018-10-20 07:16:31'),
(24, 2, 14, 1, 0, 0, 0, '2018-10-20 07:16:31', '2018-10-20 07:16:31'),
(25, 1, 16, 1, 1, 1, 1, '2018-10-20 07:22:14', '2018-10-20 07:22:14'),
(26, 1, 17, 1, 1, 1, 1, '2018-10-22 01:48:38', '2018-10-22 01:48:38');

-- --------------------------------------------------------

--
-- Table structure for table `role_module_fields`
--

CREATE TABLE `role_module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `field_id` int(10) UNSIGNED NOT NULL,
  `access` enum('invisible','readonly','write') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module_fields`
--

INSERT INTO `role_module_fields` (`id`, `role_id`, `field_id`, `access`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'write', '2018-10-18 05:51:53', '2018-10-18 05:51:53'),
(2, 1, 2, 'write', '2018-10-18 05:51:53', '2018-10-18 05:51:53'),
(3, 1, 3, 'write', '2018-10-18 05:51:53', '2018-10-18 05:51:53'),
(4, 1, 4, 'write', '2018-10-18 05:51:53', '2018-10-18 05:51:53'),
(5, 1, 5, 'write', '2018-10-18 05:51:53', '2018-10-18 05:51:53'),
(6, 1, 6, 'write', '2018-10-18 05:51:54', '2018-10-18 05:51:54'),
(7, 1, 7, 'write', '2018-10-18 05:51:54', '2018-10-18 05:51:54'),
(8, 1, 8, 'write', '2018-10-18 05:51:54', '2018-10-18 05:51:54'),
(9, 1, 9, 'write', '2018-10-18 05:51:54', '2018-10-18 05:51:54'),
(10, 1, 10, 'write', '2018-10-18 05:51:54', '2018-10-18 05:51:54'),
(11, 1, 11, 'write', '2018-10-18 05:51:54', '2018-10-18 05:51:54'),
(12, 1, 12, 'write', '2018-10-18 05:51:54', '2018-10-18 05:51:54'),
(13, 1, 13, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(14, 1, 14, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(15, 1, 15, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(16, 1, 16, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(17, 1, 17, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(18, 1, 18, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(19, 1, 19, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(20, 1, 20, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(21, 1, 21, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(22, 1, 22, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(23, 1, 23, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(24, 1, 24, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(25, 1, 25, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(26, 1, 26, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(27, 1, 27, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(28, 1, 28, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(29, 1, 29, 'write', '2018-10-18 05:51:55', '2018-10-18 05:51:55'),
(30, 1, 30, 'write', '2018-10-18 05:51:56', '2018-10-18 05:51:56'),
(31, 1, 31, 'write', '2018-10-18 05:51:56', '2018-10-18 05:51:56'),
(32, 1, 32, 'write', '2018-10-18 05:51:56', '2018-10-18 05:51:56'),
(33, 1, 33, 'write', '2018-10-18 05:51:56', '2018-10-18 05:51:56'),
(34, 1, 34, 'write', '2018-10-18 05:51:56', '2018-10-18 05:51:56'),
(46, 1, 46, 'write', '2018-10-18 05:51:57', '2018-10-18 05:51:57'),
(47, 1, 47, 'write', '2018-10-18 05:51:57', '2018-10-18 05:51:57'),
(48, 1, 48, 'write', '2018-10-18 05:51:57', '2018-10-18 05:51:57'),
(49, 1, 49, 'write', '2018-10-18 05:51:57', '2018-10-18 05:51:57'),
(50, 1, 50, 'write', '2018-10-18 05:51:57', '2018-10-18 05:51:57'),
(51, 1, 51, 'write', '2018-10-18 05:51:57', '2018-10-18 05:51:57'),
(66, 1, 66, 'write', '2018-10-20 06:41:23', '2018-10-20 06:41:23'),
(67, 1, 67, 'write', '2018-10-20 06:41:55', '2018-10-20 06:41:55'),
(68, 1, 68, 'write', '2018-10-20 06:42:23', '2018-10-20 06:42:23'),
(69, 1, 69, 'write', '2018-10-20 06:43:08', '2018-10-20 06:43:08'),
(74, 2, 1, 'readonly', '2018-10-20 07:16:27', '2018-10-20 07:16:27'),
(75, 2, 2, 'readonly', '2018-10-20 07:16:27', '2018-10-20 07:16:27'),
(76, 2, 3, 'readonly', '2018-10-20 07:16:27', '2018-10-20 07:16:27'),
(77, 2, 4, 'readonly', '2018-10-20 07:16:27', '2018-10-20 07:16:27'),
(78, 2, 5, 'readonly', '2018-10-20 07:16:27', '2018-10-20 07:16:27'),
(79, 2, 6, 'readonly', '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(80, 2, 7, 'readonly', '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(81, 2, 8, 'readonly', '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(82, 2, 9, 'readonly', '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(83, 2, 10, 'readonly', '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(84, 2, 11, 'readonly', '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(85, 2, 12, 'readonly', '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(86, 2, 13, 'readonly', '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(87, 2, 14, 'readonly', '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(88, 2, 15, 'readonly', '2018-10-20 07:16:28', '2018-10-20 07:16:28'),
(89, 2, 16, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(90, 2, 17, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(91, 2, 18, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(92, 2, 19, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(93, 2, 20, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(94, 2, 21, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(95, 2, 22, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(96, 2, 23, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(97, 2, 24, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(98, 2, 25, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(99, 2, 26, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(100, 2, 27, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(101, 2, 28, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(102, 2, 29, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(103, 2, 30, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(104, 2, 31, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(105, 2, 32, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(106, 2, 33, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(107, 2, 34, 'readonly', '2018-10-20 07:16:29', '2018-10-20 07:16:29'),
(119, 2, 46, 'readonly', '2018-10-20 07:16:30', '2018-10-20 07:16:30'),
(120, 2, 47, 'readonly', '2018-10-20 07:16:30', '2018-10-20 07:16:30'),
(121, 2, 48, 'readonly', '2018-10-20 07:16:30', '2018-10-20 07:16:30'),
(122, 2, 49, 'readonly', '2018-10-20 07:16:31', '2018-10-20 07:16:31'),
(123, 2, 50, 'readonly', '2018-10-20 07:16:31', '2018-10-20 07:16:31'),
(124, 2, 51, 'readonly', '2018-10-20 07:16:31', '2018-10-20 07:16:31'),
(125, 2, 66, 'readonly', '2018-10-20 07:16:31', '2018-10-20 07:16:31'),
(126, 2, 67, 'readonly', '2018-10-20 07:16:31', '2018-10-20 07:16:31'),
(127, 2, 68, 'readonly', '2018-10-20 07:16:31', '2018-10-20 07:16:31'),
(128, 2, 69, 'readonly', '2018-10-20 07:16:31', '2018-10-20 07:16:31'),
(129, 1, 74, 'write', '2018-10-20 07:22:08', '2018-10-20 07:22:08'),
(130, 1, 75, 'write', '2018-10-20 07:22:42', '2018-10-20 07:22:42'),
(132, 1, 77, 'invisible', '2018-10-20 07:25:44', '2018-10-20 07:25:44'),
(133, 1, 78, 'write', '2018-10-20 07:26:25', '2018-10-20 07:26:25'),
(134, 1, 79, 'invisible', '2018-10-20 07:27:34', '2018-10-20 07:27:34'),
(135, 1, 80, 'write', '2018-10-20 07:28:59', '2018-10-20 07:28:59'),
(136, 1, 81, 'write', '2018-10-22 01:48:34', '2018-10-22 01:48:34');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(3, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `name`, `path`, `extension`, `caption`, `user_id`, `hash`, `public`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '3.PNG', 'C:\\Users\\Jeerapat Sripumngoen\\Desktop\\se\\la1\\storage\\uploads\\2018-10-20-130630-3.PNG', 'PNG', '', 1, 'etwjgt9s7naaqmdaibpb', 0, '2018-10-20 06:06:51', '2018-10-20 06:06:30', '2018-10-20 06:06:51'),
(2, 'RaiNgan-2.jpg', 'C:\\Users\\Jeerapat Sripumngoen\\Desktop\\se\\la1\\storage\\uploads\\2018-10-20-130655-RaiNgan-2.jpg', 'jpg', '', 1, 'jhpvex0gzi2oy8rjoxbe', 0, '2018-10-20 06:07:00', '2018-10-20 06:06:55', '2018-10-20 06:07:00'),
(3, 'พื้นใส.png', 'C:\\Users\\Jeerapat Sripumngoen\\Desktop\\se\\la1\\storage\\uploads\\2018-10-20-130706-พื้นใส.png', 'png', '', 1, '9xlfo6dwicosxh9y7d7j', 0, '2018-10-21 01:53:08', '2018-10-20 06:07:06', '2018-10-21 01:53:08'),
(4, '1.PNG', 'C:\\Users\\Jeerapat Sripumngoen\\Desktop\\se\\la1\\storage\\uploads\\2018-10-20-131259-1.PNG', 'PNG', '', 1, 'rakap4odbcycp4itw9ir', 0, '2018-10-20 06:13:05', '2018-10-20 06:12:59', '2018-10-20 06:13:05'),
(5, 'helps.pdf', 'C:\\Users\\Jeerapat Sripumngoen\\whatres\\storage\\uploads\\2018-10-27-114700-helps.pdf', 'pdf', '', 1, 'jehbomvzbjxavaqug9ca', 0, NULL, '2018-10-27 04:47:00', '2018-10-27 04:47:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `context_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Employee',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `context_id`, `email`, `password`, `type`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, 'jeerapatsepu@gmail.com', '$2y$10$GTOMXSdpFIks3GRUN3aNd.HzeRpZ80.OnynbC/hviXlOTp5lA14Vm', 'Employee', 'UUWuJ8laTsSYcgcU8lJIRAnQHr8D25vnMbdaG2qglawu3RWD2tMCcUVXiqMA', NULL, '2018-10-18 05:52:57', '2018-10-27 04:32:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backups`
--
ALTER TABLE `backups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `backups_name_unique` (`name`),
  ADD UNIQUE KEY `backups_file_name_unique` (`file_name`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_name_unique` (`name`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`),
  ADD KEY `employees_dept_foreign` (`dept`);

--
-- Indexes for table `helps`
--
ALTER TABLE `helps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `la_configs`
--
ALTER TABLE `la_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `la_menus`
--
ALTER TABLE `la_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `memos`
--
ALTER TABLE `memos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_fields`
--
ALTER TABLE `module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_fields_module_foreign` (`module`),
  ADD KEY `module_fields_field_type_foreign` (`field_type`);

--
-- Indexes for table `module_field_types`
--
ALTER TABLE `module_field_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD KEY `roles_parent_foreign` (`parent`),
  ADD KEY `roles_dept_foreign` (`dept`);

--
-- Indexes for table `role_module`
--
ALTER TABLE `role_module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_role_id_foreign` (`role_id`),
  ADD KEY `role_module_module_id_foreign` (`module_id`);

--
-- Indexes for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_fields_role_id_foreign` (`role_id`),
  ADD KEY `role_module_fields_field_id_foreign` (`field_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploads_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backups`
--
ALTER TABLE `backups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `helps`
--
ALTER TABLE `helps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `la_configs`
--
ALTER TABLE `la_configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `la_menus`
--
ALTER TABLE `la_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `memos`
--
ALTER TABLE `memos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `module_fields`
--
ALTER TABLE `module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `module_field_types`
--
ALTER TABLE `module_field_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_module`
--
ALTER TABLE `role_module`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`);

--
-- Constraints for table `module_fields`
--
ALTER TABLE `module_fields`
  ADD CONSTRAINT `module_fields_field_type_foreign` FOREIGN KEY (`field_type`) REFERENCES `module_field_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `module_fields_module_foreign` FOREIGN KEY (`module`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `roles_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `roles` (`id`);

--
-- Constraints for table `role_module`
--
ALTER TABLE `role_module`
  ADD CONSTRAINT `role_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD CONSTRAINT `role_module_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `module_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_fields_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `uploads`
--
ALTER TABLE `uploads`
  ADD CONSTRAINT `uploads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
